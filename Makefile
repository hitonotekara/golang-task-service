.PHONY: run-local-prod run-local-dev

run-local-prod:
	export TASK_SERVICE_HTTP_PORT=8080 && \
    export TASK_SERVICE_ENV=prod && \
    go run main.go

run-local-dev:
	export TASK_SERVICE_HTTP_PORT=8080 && \
    export TASK_SERVICE_ENV=dev && \
    go run main.go
