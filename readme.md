Зависимости: \
github.com/gorilla/mux

Переменные окружения, необходимые приложению: \
TASK_SERVICE_ENV=prod # prod|dev|test \
TASK_SERVICE_HTTP_PORT=8080

Запуск приложения с установкой переменных окружения: \
make run-local-prod - запустить локально на порту 8080 в продакшен-режиме \
make run-local-dev - запустить локально на порту 8080 в девелоп-режиме