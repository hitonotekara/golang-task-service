package repository

import "task-service/internal/model"

type TaskRepositorer interface {
	FindAll() map[int]model.Task
	Add(entity *model.Task) int
	GetById(id int) (model.Task, error)
	DeleteById(id int) bool
}
