package repository

import (
	"sync"
	"task-service/internal/model"
	"task-service/internal/repository/database"
)

type TaskInMapRepository struct {
	db *database.TaskInMapDatabase
}

var repository *TaskInMapRepository
var once sync.Once

func GetTaskInMapRepository() *TaskInMapRepository {
	once.Do(func() {
		repository = &TaskInMapRepository{
			database.NewTaskInMapDatabase(),
		}
	})

	return repository
}

func (r TaskInMapRepository) FindAll() map[int]model.Task {
	return r.db.SelectAll()
}

func (r TaskInMapRepository) Add(entity *model.Task) int {
	return r.db.Insert(entity)
}

func (r TaskInMapRepository) GetById(id int) (model.Task, error) {
	return r.db.SelectOne(id)
}

func (r TaskInMapRepository) DeleteById(id int) bool {
	return r.db.DeleteOne(id)
}
