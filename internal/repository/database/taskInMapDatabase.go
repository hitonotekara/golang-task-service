package database

import (
	"errors"
	"fmt"
	"sync"
	"task-service/internal/model"
)

type TaskInMapDatabase struct {
	storage map[int]model.Task
	counter int
	mux     sync.Mutex
}

func NewTaskInMapDatabase() *TaskInMapDatabase {
	return &TaskInMapDatabase{
		storage: make(map[int]model.Task),
		counter: 0,
	}
}

func (db *TaskInMapDatabase) SelectAll() map[int]model.Task {
	return db.storage
}

func (db *TaskInMapDatabase) Insert(entity *model.Task) int {
	db.mux.Lock()
	defer db.mux.Unlock()

	db.counter++
	entity.ID = db.counter

	db.storage[db.counter] = *entity

	return db.counter
}

func (db *TaskInMapDatabase) SelectOne(id int) (model.Task, error) {
	db.mux.Lock()
	defer db.mux.Unlock()

	_, ok := db.storage[id]
	if !ok {
		return model.Task{}, errors.New(fmt.Sprintf("Can`t get Task with ID = %d\n", id))
	}

	return db.storage[id], nil
}

func (db *TaskInMapDatabase) DeleteOne(id int) bool {
	db.mux.Lock()
	defer db.mux.Unlock()

	_, ok := db.storage[id]
	if !ok {
		return false
	}
	delete(db.storage, id)

	return true
}
