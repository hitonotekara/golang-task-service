package service

import (
	"errors"
	"sync"
	"task-service/internal/model"
	"task-service/internal/repository"
)

type TaskService struct {
	repository repository.TaskRepositorer
}

var service *TaskService
var once sync.Once

func GetTaskService(repository repository.TaskRepositorer) *TaskService {
	once.Do(func() {
		service = &TaskService{repository}
	})

	return service
}

func (service *TaskService) FindAll() map[int]model.Task {
	return service.repository.FindAll()
}

func (service *TaskService) Add(entity *model.Task) (int, error) {
	taskId := service.repository.Add(entity)
	if !(taskId > 0) {
		err := errors.New("Generated Task ID less then 1")

		return 0, err
	}

	return taskId, nil
}

func (service *TaskService) GetById(id int) (model.Task, error) {
	return service.repository.GetById(id)
}

func (service *TaskService) DeleteById(id int) bool {
	return service.repository.DeleteById(id)
}
