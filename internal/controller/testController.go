package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type testResponse struct {
	Headers http.Header
	Body    string
}

func Test(w http.ResponseWriter, req *http.Request) {
	bodyByte, err := ioutil.ReadAll(req.Body)
	if err != nil {
		errorMessage := fmt.Sprintf("Error reading body: %s", err.Error())
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusBadRequest)

		return
	}

	response := testResponse{
		req.Header,
		fmt.Sprintf("%s", bodyByte),
	}

	log.Println(response)

	jsonAnswer, err := json.Marshal(response)
	if err != nil {
		errorMessage := fmt.Sprintf("Serialize test answer error: %v", err)
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusInternalServerError)

		return
	}

	io.WriteString(w, string(jsonAnswer))
}
