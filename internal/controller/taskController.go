package controller

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"task-service/internal/app/locator"
	"task-service/internal/model"
)

var serviceLocator = locator.GetServiceLocator()
var taskService = serviceLocator.TaskService

func TaskFetch(w http.ResponseWriter, req *http.Request) {
	jsonTaskList, err := json.Marshal(taskService.FindAll())
	if err != nil {
		errorMessage := fmt.Sprintf("Fetched Task serialize error: %v", err)
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusInternalServerError)

		return
	}

	io.WriteString(w, string(jsonTaskList))
}

func TaskAdd(w http.ResponseWriter, req *http.Request) {
	var task model.Task

	bodyByte, err := ioutil.ReadAll(req.Body)
	if err != nil {
		errorMessage := fmt.Sprintf("Error reading body: %s", err.Error())
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusBadRequest)

		return
	}

	err = json.Unmarshal(bodyByte, &task)
	if err != nil {
		errorMessage := fmt.Sprintf("Deserialize body error: %s", err.Error())
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusInternalServerError)

		return
	}

	_, err = taskService.Add(&task)
	if err != nil {
		errorMessage := fmt.Sprintf("Save Task error: %v", err)
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusInternalServerError)

		return
	}

	response, err := serviceLocator.TaskExecutor.Execute(&task)
	if err != nil {
		errorMessage := fmt.Sprintf("Executing task error: %s", err.Error())
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusInternalServerError)

		return
	}

	jsonAnswer, _ := json.Marshal(response)
	io.WriteString(w, string(jsonAnswer))
}

func TaskGet(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorMessage := fmt.Sprintf("Id parameter read error: %v", err)
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusBadRequest)

		return
	}

	task, err := taskService.GetById(id)
	if err != nil {
		io.WriteString(w, fmt.Sprintf("%v", err))
	} else {
		jsonTask, _ := json.Marshal(task)
		io.WriteString(w, string(jsonTask))
	}
}

func TaskDelete(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		errorMessage := fmt.Sprintf("Id parameter read error: %v", err)
		log.Println(errorMessage)
		http.Error(w, errorMessage, http.StatusBadRequest)

		return
	}

	success := taskService.DeleteById(id)
	if success {
		io.WriteString(w, fmt.Sprintf("DELETE Task with ID = %d\n", id))
	} else {
		io.WriteString(w, fmt.Sprintf("Can`t DELETE Task with ID = %d\n", id))
	}
}
