package responseDTO

import "net/http"

type CompletedTask struct {
	TaskId  int
	Status  string
	Headers http.Header
	Length  int64
}

func NewCompletedTask(id int, status string, headers http.Header, length int64) *CompletedTask {
	return &CompletedTask{
		TaskId:  id,
		Status:  status,
		Headers: headers,
		Length:  length,
	}
}
