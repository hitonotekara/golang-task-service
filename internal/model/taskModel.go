package model

type Task struct {
	ID      int
	Method  string              `json:"method"`
	Address string              `json:"address"`
	Headers map[string][]string `json:"headers"`
	Body    string              `json:"body"`
}
