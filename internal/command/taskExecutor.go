package command

import (
	"net/http"
	"strings"
	"sync"
	dto "task-service/internal/controller/responseDTO"
	"task-service/internal/model"
)

type TaskExecutor struct {
	httpClient *http.Client
}

var executor *TaskExecutor
var once sync.Once

func GetTaskExecutor(client *http.Client) *TaskExecutor {
	once.Do(func() {
		executor = &TaskExecutor{
			client,
		}
	})

	return executor
}

func (executor *TaskExecutor) Execute(task *model.Task) (*dto.CompletedTask, error) {
	clientRequest, err := http.NewRequest(task.Method, task.Address, strings.NewReader(task.Body))
	if err != nil {
		return nil, err
	}

	if len(task.Headers) > 0 {
		for headerName, headerValue := range task.Headers {
			clientRequest.Header.Add(headerName, headerValue[0])
		}
	}

	clientResponse, err := executor.httpClient.Do(clientRequest)
	if err != nil {
		return nil, err
	}

	response := dto.NewCompletedTask(
		task.ID,
		clientResponse.Status,
		clientResponse.Header,
		clientResponse.ContentLength,
	)

	return response, err
}
