package locator

import (
	"net/http"
	"task-service/internal/app"
	"task-service/internal/command"
	"task-service/internal/repository"
	"task-service/internal/service"
)

type serviceLocator struct {
	App          *app.App
	TaskService  *service.TaskService
	TaskExecutor *command.TaskExecutor
}

var locator *serviceLocator

func init() {
	locator = &serviceLocator{
		app.GetApp(),
		service.GetTaskService(repository.GetTaskInMapRepository()),
		command.GetTaskExecutor(&http.Client{}),
	}
}

func GetServiceLocator() *serviceLocator {
	return locator
}
