package server

import (
	"log"
	"net/http"
	"sync"
	"task-service/internal/app/interrupter"
	"task-service/internal/app/server/router"
)

type WebServer struct {
	Server *http.Server
}

var server *WebServer
var once sync.Once

func GetWebServer(srv *http.Server) *WebServer {
	once.Do(func() {
		server = &WebServer{Server: srv}
	})

	return server
}

func (srv *WebServer) StartWebServer() {
	iAmInterrupter := interrupter.Start(srv.Server)

	router.InitRoutes()
	log.Fatal(srv.Server.ListenAndServe())

	iAmInterrupter.Exit()
}
