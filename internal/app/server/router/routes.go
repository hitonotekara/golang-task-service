package router

import (
	"github.com/gorilla/mux"
	"net/http"
	"task-service/internal/app/locator"
	"task-service/internal/controller"
)

func InitRoutes() {
	r := mux.NewRouter()

	r.HandleFunc("/task", controller.TaskFetch).Methods("GET")
	r.HandleFunc("/task", controller.TaskAdd).Methods("POST")
	r.HandleFunc("/task/{id:[0-9]+}", controller.TaskGet).Methods("GET")
	r.HandleFunc("/task/{id:[0-9]+}", controller.TaskDelete).Methods("DELETE")

	if !locator.GetServiceLocator().App.IsProd() {
		r.HandleFunc("/test", controller.Test)
	}

	http.Handle("/", r)
}
