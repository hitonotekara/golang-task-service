package interrupter

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
)

type Interrupter struct {
	channel chan struct{}
}

func Start(srv *http.Server) *Interrupter {
	iAmInterrupter := &Interrupter{make(chan struct{})}
	idleConnectionsClosed := iAmInterrupter.channel

	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		if err := srv.Shutdown(context.Background()); err != nil {
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnectionsClosed)
	}()

	return iAmInterrupter
}

func (i Interrupter) Exit() {
	<-i.channel
}
