package app

import (
	"log"
	"os"
)

type App struct {
	env string
}

var app *App

func init() {
	env := os.Getenv("TASK_SERVICE_ENV")
	if env == "" {
		log.Fatalln("Environment variable TASK_SERVICE_ENV is not set")
	}

	app = &App{
		env,
	}
}

func GetApp() *App {
	return app
}

func (app *App) IsProd() bool {
	return app.env == "prod"
}
