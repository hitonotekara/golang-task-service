package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"task-service/internal/app/server"
)

var webServer *server.WebServer

func init() {
	httpPort := os.Getenv("TASK_SERVICE_HTTP_PORT")
	if httpPort == "" {
		log.Fatalln("Environment variable TASK_SERVICE_HTTP_PORT is not set")
	}

	webServer = server.GetWebServer(&http.Server{
		Addr: fmt.Sprintf(":%s", httpPort),
	})
}

func main() {
	webServer.StartWebServer()
}
